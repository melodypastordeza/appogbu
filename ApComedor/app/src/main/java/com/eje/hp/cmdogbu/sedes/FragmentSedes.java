package com.eje.hp.cmdogbu.sedes;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.eje.hp.cmdogbu.Options;
import com.eje.hp.cmdogbu.R;
import com.eje.hp.cmdogbu.Util;
import com.eje.hp.cmdogbu.clases.PanelPublicidad;
import com.eje.hp.cmdogbu.clases.Tickets;
import com.eje.hp.cmdogbu.mostrar_ticket.FragmentMostrarT;
import com.eje.hp.cmdogbu.reservar_ticket.FragmentReservarT;
import com.journeyapps.barcodescanner.ViewfinderView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentSedes extends android.support.v4.app.Fragment implements FragmentReservarT.OnFragmentInteractionListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    View view;
    private int suma1 = 0;
    static int suma2 = 0;
    ProgressDialog progreso;
    RecyclerView recyclerView;
    List<ListaSedes> ListSedes = new ArrayList<>();
    public static final int MY_DEFAULT_TIMEOUT = 150000;

    //private ViewGroup linearLayoutDetails;
    //private ImageView imageViewExpand;
    private static final int DURATION = 250;

    public FragmentSedes() {
        // Required empty public constructor
    }

    public static FragmentSedes newInstance(String param1, String param2) {
        FragmentSedes fragment = new FragmentSedes();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_fragment_sedes, container, false);
        Activity ac = getActivity();
        if (ac != null) ac.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        try {
            SubirTickets();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void SubirLista(View view) {
        ListSedes.add(new ListaSedes("Ciudad universitaria", R.mipmap.ciudaduni, suma1, suma2));
        ListSedes.add(new ListaSedes("Cangallo", R.mipmap.sanfernando, 0, 0));
        ListSedes.add(new ListaSedes("San Juan de Lurigancho", R.mipmap.sanjuan, 0, 0));
        ListSedes.add(new ListaSedes("Veterinaria", R.mipmap.veterinaria, 0, 0));
        recyclerView = view.findViewById(R.id.recycler_view_sedes);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerViewAdapter a = new RecyclerViewAdapter();
        recyclerView.setAdapter(a);
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private CardView mCardView;
        private ImageView mImageView;
        private TextView txtComedor, txtCantitadTo, txtCantidadToApp;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
        }


        public RecyclerViewHolder(LayoutInflater inflater,final ViewGroup container) {
            super(inflater.inflate(com.eje.hp.cmdogbu.R.layout.card_view_sedes, container, false));

            //imageViewExpand = itemView.findViewById(R.id.imageViewExpand);
            //linearLayoutDetails = itemView.findViewById(R.id.linearLayoutDetails);
            mCardView = itemView.findViewById(R.id.card_view_sedes);
            mImageView = itemView.findViewById(R.id.ImageSede);
            txtComedor = itemView.findViewById(R.id.txtComedor);
            txtCantidadToApp = itemView.findViewById(R.id.txtTicketApp);
            txtCantitadTo = itemView.findViewById(R.id.txtTicketTotal);

            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int i = recyclerView.getChildAdapterPosition(v);

                    if (i == 0) {
                        FragmentReservarT fragment = new FragmentReservarT();
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.replace(R.id.content_option, fragment);
                        ft.commit();
                    } else if (i == 1) {
                        getToast(container);
                        //Toast.makeText(getContext(), "Estamos trabajando", Toast.LENGTH_LONG).show();
                    } else if (i == 2) {
                        getToast(container);
                        //Toast.makeText(getContext(), "Estamos trabajando", Toast.LENGTH_LONG).show();
                    } else if (i == 3) {
                        getToast(container);
                        //Toast.makeText(getContext(), "Estamos trabajando", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

   public class RecyclerViewAdapter extends RecyclerView.Adapter<FragmentSedes.RecyclerViewHolder> implements View.OnClickListener {

        private Context context;
        private View.OnClickListener listener;

        public RecyclerViewAdapter() {
        }

        @NonNull
        @Override
        public FragmentSedes.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            context = viewGroup.getContext();
            viewGroup.setOnClickListener(this);
            return new FragmentSedes.RecyclerViewHolder(inflater, viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull FragmentSedes.RecyclerViewHolder holder, int i) {
            Picasso.get().load(ListSedes.get(i).getImg()).fit().into(holder.mImageView);
            holder.mImageView.setImageResource(ListSedes.get(i).getImg());
            Picasso.get().load(ListSedes.get(i).getSede());
            holder.txtComedor.setText(ListSedes.get(i).getSede());
            holder.txtCantitadTo.setText("Cantidad de ticket por comedor : " + ListSedes.get(i).getCantidadTotal());
            holder.txtCantidadToApp.setText("Cantidad de ticket por aplicación : " + ListSedes.get(i).getCantidadTotalApp());


        }

        @Override
        public int getItemCount() {
            return ListSedes.size();
        }

        public void setOnClickListener(View.OnClickListener listener) {
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onClick(v);
            }
        }
    }

    private void SubirTickets() throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestST = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequestST;
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov = s.Despublicidad(Util.getSexo(prefs), Util.getGenero(prefs));
        String add = mov + "subirTicket.php";
        jsonObjectRequestST = new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Tickets[] ti = new Tickets[10];
                JSONArray json = response.optJSONArray("ticket");
                try {
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject jsonObject;
                        jsonObject = json.getJSONObject(i);
                        ti[i] = new Tickets();
                        ti[i].setTi_id(jsonObject.optInt("ti_id"));
                        ti[i].setTi_nivel(jsonObject.optInt("ti_nivel"));
                        ti[i].setTi_turno(jsonObject.optInt("ti_turno"));
                        ti[i].setTi_cantidad(jsonObject.optInt("ti_cantidad"));
                    }
                    ItemsLista(ti);
                    progreso.dismiss();
                    SubirTotalApp();

                } catch (JSONException e) {
                    progreso.dismiss();
                    Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    progreso.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestST.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestST.add(jsonObjectRequestST);
    }

    public void ItemsLista(Tickets tickets[]) {
        for (int i = 0; i < tickets.length; i++) {
            if (tickets[i].getTi_cantidad() > 0) {
                suma1 = suma1 + tickets[i].getTi_cantidad();
            }
        }
    }

    public void RestarCantidad(int cantidadtotal,int cantidadconsumida){
        suma2 = cantidadtotal-cantidadconsumida;
    }

    private void SubirTotalApp() throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestAC = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequestAC;
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"SubirCantidadApp.php";
        add=add.replace(" ","%20");
        jsonObjectRequestAC= new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("app_id");
                try {
                    JSONObject jsonObject;
                    jsonObject = json.getJSONObject(0);
                    int app_cantidadtotal = jsonObject.optInt("app_cantidadtotal");
                    int app_cantidadconsumida =jsonObject.optInt("app_cantidadconsumida");
                    RestarCantidad(app_cantidadtotal,app_cantidadconsumida);
                    SubirLista(view);
                    progreso.dismiss();
                } catch (JSONException e) {
                    progreso.dismiss();
                    Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    progreso.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestAC.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestAC.add(jsonObjectRequestAC);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void getToast(ViewGroup container){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_image,container,false);
        Toast toast = new Toast(getContext());
        toast.setGravity(Gravity.CLIP_HORIZONTAL,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }
    /*
    public void toggleDetails() {
        if (linearLayoutDetails.getVisibility() == View.GONE) {
            ExpandAndCollapseViewUtil.expand(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.ic_expand_more_black_24dp);
            rotate(-180.0f);
        }else{
            ExpandAndCollapseViewUtil.collapse(linearLayoutDetails, DURATION);
            imageViewExpand.setImageResource(R.drawable.ic_expand_less_black_24dp);
            rotate(180.0f);
        }
    }

    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        imageViewExpand.startAnimation(animation);
    }*/
}
