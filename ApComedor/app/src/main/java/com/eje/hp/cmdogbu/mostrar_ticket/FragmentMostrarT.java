package com.eje.hp.cmdogbu.mostrar_ticket;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eje.hp.cmdogbu.Util;
import com.eje.hp.cmdogbu.clases.PanelPublicidad;
import com.eje.hp.cmdogbu.clases.Usuarios;
import com.eje.hp.cmdogbu.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentMostrarT.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentMostrarT#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentMostrarT extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    boolean flag=true;
    TextView txtNombreyApellido,txtCodigo,txtFecha,txtTurno,txtNivel,txtNumTicket;
    Usuarios U = null;
    ProgressDialog progreso;
    ImageView image,image2,image3,image4;
    public static final int MY_DEFAULT_TIMEOUT = 150000;

    private Typeface ligth;


    public FragmentMostrarT() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMostrarT.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentMostrarT newInstance(String param1, String param2) {
        FragmentMostrarT fragment = new FragmentMostrarT();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_fragment_mostrar_t, container, false);
        Activity a = getActivity();
        if(a != null) a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        InicializarComponentes(rootView);
        try {
            ConsultarReserva();

        } catch (Exception e) {
            e.printStackTrace();
        }
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast(container);
            }
        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;

    }

    public void Toast(ViewGroup vg){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_qr,vg,false);
        Toast toast = new Toast(getContext());
        toast.setGravity(Gravity.CLIP_HORIZONTAL,0,0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void InicializarComponentes(View rootView){
        String fuente = "fuentes/ligth.otf";
        image = (ImageView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.image);
        image2 = (ImageView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.codigoBarra);
        image3= (ImageView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.codigoQR);
        image4= (ImageView) rootView.findViewById(R.id.toastQR) ;
        txtNombreyApellido = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtNombreyApellido);
        txtNombreyApellido.setTypeface(ligth);
        txtCodigo = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtCodigoU);
        txtCodigo.setTypeface(ligth);
        txtFecha = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtFecha);
        txtFecha.setTypeface(ligth);
        txtTurno = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtTurno);
        txtTurno.setTypeface(ligth);
        txtNivel = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtNivel);
        txtNivel.setTypeface(ligth);
        txtNumTicket = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtNumTicket);
        txtNumTicket.setTypeface(ligth);

        TextView txtNA = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtNA);
        txtNA.setTypeface(ligth);
        TextView txtCu = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtCu);
        txtCu.setTypeface(ligth);
        TextView txtF = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtF);
        txtF.setTypeface(ligth);
        TextView txtT = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtT);
        txtT.setTypeface(ligth);
        TextView txtN = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtN);
        txtN.setTypeface(ligth);
        TextView txtNT = (TextView) rootView.findViewById(com.eje.hp.cmdogbu.R.id.txtNt);
        txtNT.setTypeface(ligth);
    }


    private void ConsultarReserva() throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestCR = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequestCR;
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"mostrarUsuario.php?ctr_fkAlumno="+Util.getUsuario(prefs)+"&ctr_fkServicio="+12;
        jsonObjectRequestCR = new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
                JSONArray json = response.optJSONArray("control_2019");
                U = new Usuarios();
                try {
                    JSONObject jsonObject;
                    jsonObject = json.getJSONObject(0);
                    U.setNivel(jsonObject.optInt("ctr_Nivel"));
                    U.setTurno(jsonObject.optInt("ctr_Turno"));
                    U.setFecha(jsonObject.optString("ctr_Fecha"));
                    U.setNumTicket(jsonObject.optString("ctr_NumTicket"));
                    progreso.dismiss();
                    SubirTextos();
                } catch (JSONException e) {
                    Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                    progreso.dismiss();
                } catch (Exception e) {
                    progreso.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestCR.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestCR.add(jsonObjectRequestCR);

    }

    public void SubirTextos() throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        SubirImagen();
        txtNombreyApellido.setText(Util.getNombres(prefs)+" "+Util.getApellidos(prefs));
        txtCodigo.setText(Util.getUsuario(prefs));
        txtFecha.setText(U.getFecha());
        txtNumTicket.setText(U.getNumTicket());
        //txtCorreo.setText(U.getCorreo());
        if(U.getNivel()==1){
            txtNivel.setText("Nivel 1");
            if(U.getTurno()==1){
                txtTurno.setText("Turno 1 / 12:00-12:20");
            }else if(U.getTurno()==2){
                txtTurno.setText("Turno 2 / 12:20-12:40");
            }
            else if(U.getTurno()==3){
                txtTurno.setText("Turno 3 / 12:40-1:00");
            }
            else if(U.getTurno()==4){
                txtTurno.setText("Turno 4 / 1:00-1:20");
            }
            else if(U.getTurno()==5){
                txtTurno.setText("Turno 5 / 1:20-1:40");
            }
        }else if(U.getNivel()==2){
            txtNivel.setText("Nivel 2");
            if(U.getTurno()==1){
                txtTurno.setText("Turno 1 / 12:00-12:20");
            }else if(U.getTurno()==2){
                txtTurno.setText("Turno 2 / 12:20-12:40");
            }
            else if(U.getTurno()==3){
                txtTurno.setText("Turno 3 / 12:40-1:00");
            }
            else if(U.getTurno()==4){
                txtTurno.setText("Turno 4 / 1:00-1:20");
            }
            else if(U.getTurno()==5){
                txtTurno.setText("Turno 5 / 1:20-1:40");
            }
        }
        SubirCodigoBarra(Util.getUsuario(prefs));
        SubirCodigoQR(Util.getUsuario(prefs));
    }

    private void SubirImagen() throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue request = Volley.newRequestQueue(getContext());
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String urlimagen = mov+"Fotos Alumno/"+Util.getUsuario(prefs)+".jpg";
        urlimagen = urlimagen.replace(" ","%20");

        ImageRequest imageRequest = new ImageRequest(urlimagen, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                image.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(),"Error al cargar la imagen",Toast.LENGTH_SHORT).show();
            }
        });
        request.add(imageRequest);
    }

    private void SubirCodigoBarra(String Codigo)
    {
        String teksbarcode;
        MultiFormatWriter multiFormatWriter= new MultiFormatWriter();
        teksbarcode = Codigo;
        try{
            BitMatrix bitMatrix = multiFormatWriter.encode(teksbarcode,BarcodeFormat.CODE_39,300,50);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(bitMatrix);
            image2.setImageBitmap(bitmap);
        }catch (WriterException e){
            e.printStackTrace();
        }
    }

    private void SubirCodigoQR(String Codigo)
    {
        String teksbarcode;
        MultiFormatWriter multiFormatWriter= new MultiFormatWriter();
        teksbarcode = Codigo;
        try{
            BitMatrix bitMatrix = multiFormatWriter.encode(teksbarcode,BarcodeFormat.QR_CODE,120,100);
            BarcodeEncoder encoder = new BarcodeEncoder();
            Bitmap bitmap = encoder.createBitmap(bitMatrix);
            image3.setImageBitmap(bitmap);
            image4.setImageBitmap(bitmap);
        }catch (WriterException e){
            e.printStackTrace();
        }
    }


}
