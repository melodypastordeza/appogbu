package com.eje.hp.cmdogbu;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.KeyEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eje.hp.cmdogbu.sedes.FragmentSedes;
import com.eje.hp.cmdogbu.clases.PanelPublicidad;
import com.eje.hp.cmdogbu.mostrar_ticket.FragmentMostrarT;
import com.eje.hp.cmdogbu.principal.Main;
import com.eje.hp.cmdogbu.publicidad.NavegacionPublicidad;
import com.eje.hp.cmdogbu.publicidad.RecyclerFragment;
import com.eje.hp.cmdogbu.reservar_ticket.FragmentReservarT;

import static android.view.View.TEXT_ALIGNMENT_CENTER;

public class Options extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,
        FragmentMostrarT.OnFragmentInteractionListener,
        FragmentReservarT.OnFragmentInteractionListener,
        RecyclerFragment.OnFragmentInteractionListener,
        NavegacionPublicidad.OnFragmentInteractionListener,
        FragmentSedes.OnFragmentInteractionListener {

    ImageView imagePri;
    TextView txtNombreApellido,txtCodigo;
    private SharedPreferences prefs;
    ProgressDialog progreso;
    public static boolean rsp=false;
    private Typeface ligth;
    String  activo;
    int diaactivo;
    int cantdisponible;

    public static final int MY_DEFAULT_TIMEOUT = 150000;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        prefs = getSharedPreferences("Preferences",Context.MODE_PRIVATE);


        try {
            funciona();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = new RecyclerFragment().newInstance();
        fm.beginTransaction().add(R.id.content_option,fragment).commit();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        View navHeader = navigationView.getHeaderView(0);


        InicializarComponentes(navHeader);

        try {
            ConsultarReserva();
        } catch (Exception e) {
            e.printStackTrace();
        }
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        navigationView.setBackgroundColor(Color.parseColor("#FFFFFF"));
        navigationView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        navigationView.setItemTextColor(ColorStateList.valueOf(Color.parseColor("#6d6d6d")));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void funciona() throws Exception {
        RequestQueue requestST = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequestST;
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"consultarHora.php";
        jsonObjectRequestST = new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("abierto");
                try {
                    JSONObject jsonObject;
                    jsonObject = json.getJSONObject(0);
                    activo = jsonObject.optString("activo");
                    cantdisponible= jsonObject.optInt("cantidadrestante");
                    diaactivo=jsonObject.optInt("diaactivo");
                } catch (JSONException e) {
                    Toast.makeText(Options.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Options.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestST.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestST.add(jsonObjectRequestST);

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);

        Fragment miFragment =null;
        boolean fragmentSeleccionado = false;
        boolean fragmentSelec = false;

        if (id == R.id.nav_anuncios) {
            miFragment = new RecyclerFragment();
            toolbar.setTitle("Anuncios");
            fragmentSelec = true;
        }else if (id == R.id.nav_reserva) {
            if(diaactivo==1){
                if (rsp) {
                    Toast.makeText(Options.this, "Usted cuenta con ticket reservado", Toast.LENGTH_LONG).show();
                }else {
                    if(activo.equals("1")) {
                        if(cantdisponible>0) {
                            if (!Util.getImei(prefs).equalsIgnoreCase("")) {
                                miFragment = new FragmentSedes();
                                fragmentSelec = true;
                                toolbar.setTitle("Sedes");
                            }else {
                                Toast.makeText(Options.this, "s:" + Util.getImei(prefs), Toast.LENGTH_LONG).show();
                                Toast.makeText(Options.this, "No puede reservar ticket ya que no accedio a las restricciones", Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(Options.this, "Ya no quedan tickets disponibles por este medio", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(Options.this, "No es hora de reservar ticket", Toast.LENGTH_LONG).show();
                    }
                }
            }else {
                Toast.makeText(Options.this, "Hoy no esta activa la app para reservar ticket", Toast.LENGTH_LONG).show();
            }
        }else if (id == R.id.nav_mostrar) {
            if(rsp){
                miFragment = new FragmentMostrarT();
                fragmentSeleccionado = true;
                toolbar.setTitle("Mostrar Ticket");

            }else{
                Toast.makeText(Options.this, "Usted no cuenta con ticket reservado",Toast.LENGTH_LONG).show();
            }
        }else if (id == R.id.nav_cerrar) {
            removeSharedPreferences();
            logOut();
        }
        if(fragmentSeleccionado){
            getSupportFragmentManager().beginTransaction().replace(R.id.content_option,miFragment).addToBackStack(null).commit();
        }else if(fragmentSelec){
            getSupportFragmentManager().beginTransaction().replace(R.id.content_option,miFragment).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void logOut(){
        Intent intent = new Intent(Options.this,Main.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        Options.this.startActivity(intent);
    }

    private void removeSharedPreferences(){
        prefs.edit().clear().apply();
    }



    private void ConsultarReserva() throws Exception {
        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestP = Volley.newRequestQueue(Options.this);
        JsonObjectRequest jsonObjectRequestP;
        progreso = new ProgressDialog(Options.this);
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"consultarReserva.php?ctr_fkAlumno="+Util.getUsuario(prefs)+"&ctr_fkServicio="+12;
        jsonObjectRequestP= new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("control_2019");
                try {
                    JSONObject jsonObject;
                    jsonObject = json.getJSONObject(0);
                    if(jsonObject.optInt("ctr_fkServicio")==12){
                        rsp=true ;
                    }else {
                        rsp=false;
                    }
                    progreso.hide();
                    progreso.dismiss();
                    SubirTexto();
                } catch (JSONException e) {
                    Toast.makeText(Options.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                    progreso.hide();
                    progreso.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.hide();
                progreso.dismiss();
                Toast.makeText(Options.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestP.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestP.add(jsonObjectRequestP);
    }

    public void InicializarComponentes(View navHeader){
        String fuente = "fuentes/ligth.otf";
        this.ligth = Typeface.createFromAsset(getAssets(),fuente);
        imagePri = (ImageView) navHeader.findViewById(R.id.imagePri);
        txtNombreApellido = (TextView) navHeader.findViewById(R.id.txtApellidoNombre);
        txtNombreApellido.setTypeface(ligth);
        txtCodigo = (TextView) navHeader.findViewById(R.id.CorreoIns);
        txtCodigo.setTypeface(ligth);
    }

    private void SubirTexto(){
        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        txtNombreApellido.setText(" "+Util.getNombres(prefs)+" "+Util.getApellidos(prefs));
        txtCodigo.setText(" "+Util.getUsuario(prefs));
        SubirImagen();
    }

    private void SubirImagen(){
        RequestQueue request = Volley.newRequestQueue(Options.this);
        String ip = getString(R.string.ip);
        String urlimagen = ip+"Fotos Alumno/"+Util.getUsuario(prefs)+".jpg";
        urlimagen = urlimagen.replace(" ","%20");

        ImageRequest imageRequest = new ImageRequest(urlimagen, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                imagePri.setImageBitmap(response);
            }
        }, 0, 0, ImageView.ScaleType.CENTER, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(Options.this,"Error al cargar la imagen",Toast.LENGTH_SHORT).show();
            }
        });
        request.add(imageRequest);
    }

    public boolean onKeyDown(int keyCode,KeyEvent event){
        SharedPreferences prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        if(!Util.getCheck(prefs)) {
            if (keyCode == event.KEYCODE_BACK) {
                removeSharedPreferences();
                logOut();
            }
        }
        return super.onKeyDown(keyCode,event);
    }
}
