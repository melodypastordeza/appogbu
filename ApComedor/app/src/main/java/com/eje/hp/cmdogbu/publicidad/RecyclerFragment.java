package com.eje.hp.cmdogbu.publicidad;


import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

public class RecyclerFragment extends Fragment  {

    private OnFragmentInteractionListener mListener;

    RecyclerView recyclerView;


    public static Fragment newInstance() {
        return new RecyclerFragment();
    }

    List<ListaPublicidad> mList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.eje.hp.cmdogbu.R.layout.fragment_recycler,container,false);
        Activity ac = getActivity();
        if(ac != null) ac.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mList.add(new ListaPublicidad("http://ogbu.unmsm.edu.pe",  com.eje.hp.cmdogbu.R.mipmap.comedor));
        mList.add(new ListaPublicidad("http://sum.unmsm.edu.pe/",  com.eje.hp.cmdogbu.R.mipmap.captura));
        mList.add(new ListaPublicidad("http://telematica.unmsm.edu.pe/",  com.eje.hp.cmdogbu.R.mipmap.telematica));
        mList.add(new ListaPublicidad("http://ceid.letras.unmsm.edu.pe/",  com.eje.hp.cmdogbu.R.mipmap.idioma));
        recyclerView = view.findViewById(com.eje.hp.cmdogbu.R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        RecyclerViewAdapter a = new RecyclerViewAdapter();
        recyclerView.setAdapter(a);
        return view;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private CardView mCardView;
        private ImageView mImageView;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
        }
        /*public void bind(final ListaPublicidad l){
            mTextView.setText(l.getTexrt());
        }*/

        public RecyclerViewHolder(LayoutInflater inflater,ViewGroup container){
            super(inflater.inflate(com.eje.hp.cmdogbu.R.layout.card_view,container,false));
            mCardView=itemView.findViewById(com.eje.hp.cmdogbu.R.id.card_container);
            mImageView=itemView.findViewById(com.eje.hp.cmdogbu.R.id.imagen);
            /*mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm = getFragmentManager();
                    Fragment fragment = new NavegacionPublicidad();
                    fm.beginTransaction().replace(R.id.content_option,fragment).commit();
                }
            });*/
            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentManager fm = getFragmentManager();
                    Fragment fragment = new NavegacionPublicidad();
                    ((NavegacionPublicidad) fragment).setURL(mList.get(recyclerView.getChildAdapterPosition(v)).getTexrt());
                    fm.beginTransaction().replace(com.eje.hp.cmdogbu.R.id.content_option,fragment).commit();
                }
            });
        }
    }


    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> implements View.OnClickListener{

        private Context context;
        private View.OnClickListener listener;

        public RecyclerViewAdapter( ){
        }

        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());
            context = viewGroup.getContext();
            viewGroup.setOnClickListener(this);
            return new RecyclerViewHolder(inflater, viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int i) {
            //holder.bind(mList.get(i));
            Picasso.get().load(mList.get(i).getImagen()).fit().into(holder.mImageView);
            //holder.mImageView.setImageResource(mList.get(i).getImagen());
            //holder.mTextView.setText(mList.get(i).getTexrt());
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }

        public void setOnClickListener(View.OnClickListener listener){
            this.listener=listener;
        }

        @Override
        public void onClick(View v) {
            if(listener!=null){
                listener.onClick(v);
            }
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}

