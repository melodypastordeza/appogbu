package com.eje.hp.cmdogbu.clases;

public class Usuarios{

    String Codigo;
    String ApePaterno;
    String ApeMaterno;
    String Nombre;
    String Sexo;
    int fkEscuela;
    String Dni;
    int fkSitAcademica;
    int fkEstPermanencia;
    String Observacion;
    String Contrasena;
    int Nivel;
    int Turno;
    String Fecha;
    String NumTicket;

    public Usuarios() {
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String codigo) {
        Codigo = codigo;
    }

    public String getApePaterno() {
        return ApePaterno;
    }

    public void setApePaterno(String apePaterno) {
        ApePaterno = apePaterno;
    }

    public String getApeMaterno() {
        return ApeMaterno;
    }

    public void setApeMaterno(String apeMaterno) {
        ApeMaterno = apeMaterno;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String sexo) {
        Sexo = sexo;
    }

    public int getFkEscuela() {
        return fkEscuela;
    }

    public void setFkEscuela(int fkEscuela) {
        this.fkEscuela = fkEscuela;
    }

    public String getDni() {
        return Dni;
    }

    public void setDni(String dni) {
        Dni = dni;
    }

    public int getFkSitAcademica() {
        return fkSitAcademica;
    }

    public void setFkSitAcademica(int fkSitAcademica) {
        this.fkSitAcademica = fkSitAcademica;
    }

    public int getFkEstPermanencia() {
        return fkEstPermanencia;
    }

    public void setFkEstPermanencia(int fkEstPermanencia) {
        this.fkEstPermanencia = fkEstPermanencia;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String observacion) {
        Observacion = observacion;
    }

    public String getContrasena() {
        return Contrasena;
    }

    public void setContrasena(String contrasena) {
        Contrasena = contrasena;
    }

    public int getNivel() {
        return Nivel;
    }

    public void setNivel(int nivel) {
        Nivel = nivel;
    }

    public int getTurno() {
        return Turno;
    }

    public void setTurno(int turno) {
        Turno = turno;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public String getNumTicket() {
        return NumTicket;
    }

    public void setNumTicket(String numTicket) {
        NumTicket = numTicket;
    }
}
