package com.eje.hp.cmdogbu.clases;

public class Tickets {

    int ti_id;
    int ti_nivel;
    int ti_turno;
    int ti_cantidad;

    public Tickets() {

    }

    public int getTi_id() {
        return ti_id;
    }

    public void setTi_id(int ti_id) {
        this.ti_id = ti_id;
    }

    public int getTi_nivel() {
        return ti_nivel;
    }

    public void setTi_nivel(int ti_nivel) {
        this.ti_nivel = ti_nivel;
    }

    public int getTi_turno() {
        return ti_turno;
    }

    public void setTi_turno(int ti_turno) {
        this.ti_turno = ti_turno;
    }

    public int getTi_cantidad() {
        return ti_cantidad;
    }

    public void setTi_cantidad(int ti_cantidad) {
        this.ti_cantidad = ti_cantidad;
    }
}
