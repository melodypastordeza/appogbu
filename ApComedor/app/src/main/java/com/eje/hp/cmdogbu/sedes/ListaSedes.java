package com.eje.hp.cmdogbu.sedes;

public class ListaSedes {
    private String sede;
    private int img;
    private int cantidadTotal;
    private int cantidadTotalApp;

    public ListaSedes(String sede,int img, int cantidadTotal, int cantidadTotalApp) {
        this.sede = sede;
        this.img = img;
        this.cantidadTotal = cantidadTotal;
        this.cantidadTotalApp = cantidadTotalApp;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getCantidadTotal() {
        return cantidadTotal;
    }

    public void setCantidadTotal(int cantidadTotal) {
        this.cantidadTotal = cantidadTotal;
    }

    public int getCantidadTotalApp() {
        return cantidadTotalApp;
    }

    public void setCantidadTotalApp(int cantidadTotalApp) {
        this.cantidadTotalApp = cantidadTotalApp;
    }
}
