package com.eje.hp.cmdogbu.publicidad;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eje.hp.cmdogbu.Util;
import com.eje.hp.cmdogbu.VolleySingleton;
import com.eje.hp.cmdogbu.clases.PanelPublicidad;

public class AcercaPrograma extends AppCompatActivity {

    private SharedPreferences prefs;
    ProgressDialog progreso;
    TextView AP,tituloAP,tituloHA,HA;
    private Typeface ligth;
    public static final int MY_DEFAULT_TIMEOUT = 150000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.eje.hp.cmdogbu.R.layout.activity_acerca_programa);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        prefs = getSharedPreferences("Preferences",Context.MODE_PRIVATE);
        InicializarComponentes();
        try {
            SubirTxt();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InicializarComponentes(){
        String fuente = "fuentes/ligth.otf";
        AP = (TextView)findViewById(com.eje.hp.cmdogbu.R.id.txtAP);
        AP.setTypeface(ligth);
        tituloAP = (TextView)findViewById(com.eje.hp.cmdogbu.R.id.txttituloAP);
        tituloAP.setTypeface(ligth);
        tituloHA = (TextView)findViewById(com.eje.hp.cmdogbu.R.id.txttituloHA);
        tituloHA.setTypeface(ligth);
        HA = (TextView)findViewById(com.eje.hp.cmdogbu.R.id.txtHA);
        HA.setTypeface(ligth);
    }

    private void SubirTxt() throws Exception {
        String Texto = "Usuario : Codigo universitario"+"\n"+"Contraseña : DNI"+"\n\n"+"Tener en cuenta que la aplicacion " +
                "es personal, el primer registro es el que quedará y no se podrá cambiar de usuario; además deberá aceptar los " +
                "permisos que le pida la aplicacion ya que no podrá acceder a sus funciones."+"\n\n"+"Si el usuario reserva ticket " +
                "cuatro veces y no asiste se le establecerá una sanción de un mes sin poder asistir al comedor."+"\n\n"+
                "La aplicacion funciona en forma paralela con el sistema del comedor por ello se tiene un limite de acceso a la " +
                "cantidad de tickets, una vez que se llega a ese limite solo se podrà sacar tickets de forma personal.";
        AP.setText(Texto);
        ConsultarHora();
    }

    private void ConsultarHora() throws Exception {
        JsonObjectRequest jsonObjectRequestP;
        progreso = new ProgressDialog(this);
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"consultarHora.php";
        jsonObjectRequestP= new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("abierto");
                JSONObject jsonObject=null;
                try {
                    String HI,HF;
                    int diaactivo;
                    jsonObject = json.getJSONObject(0);
                    HI = jsonObject.optString("horainicio");
                    HF = jsonObject.getString("horafinal");
                    diaactivo = jsonObject.getInt("diaactivo");
                    if(diaactivo==0){
                        HA.setText("La aplicación no está activa");
                    }else if(diaactivo == 1){
                        HA.setText(HI+" - "+HF);
                    }
                    progreso.hide();
                    progreso.dismiss();
                } catch (JSONException e) {
                    Toast.makeText(AcercaPrograma.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                    progreso.hide();
                    progreso.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.hide();
                progreso.dismiss();
                Toast.makeText(AcercaPrograma.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestP.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstanciaVolley(AcercaPrograma.this).addToRequestQueue(jsonObjectRequestP);
    }
}
