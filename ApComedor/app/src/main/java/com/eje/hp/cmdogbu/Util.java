package com.eje.hp.cmdogbu;

import android.content.SharedPreferences;

public class Util {

    public static String getUsuario(SharedPreferences preferences){
        return preferences.getString("Usuario","");
    }

    public static String getApellidos(SharedPreferences preferences){
        return preferences.getString("Apellidos","");
    }

    public static String getNombres(SharedPreferences preferences){
        return preferences.getString("Nombres","");
    }

    public static  int getfkEscuela(SharedPreferences preferences){
        return  preferences.getInt("fkEscuela",0);
    }

    public static String getDni(SharedPreferences preferences){
        return preferences.getString("Dni","");
    }

    public static  int getfkSitAcademica(SharedPreferences preferences){
        return  preferences.getInt("fkSitAcademica",0);
    }

    public static  int getfkEstPermanencia(SharedPreferences preferences){
        return  preferences.getInt("fkEstPermanencia",0);
    }

    public static String getObservacion(SharedPreferences preferences){
        return preferences.getString("Observacion","");
    }

    public static String getPassword(SharedPreferences preferences){
        return preferences.getString("Password","");
    }

    public static boolean getCheck(SharedPreferences preferences){
        return preferences.getBoolean("Check",false);
    }

    public static String getImei(SharedPreferences preferences){
        return preferences.getString("Imei","");
    }

    public static String getSexo(SharedPreferences preferences){
        return preferences.getString("Sexo","");
    }

    public static String getGenero(SharedPreferences preferences){
        return preferences.getString("Genero","");
    }
}


