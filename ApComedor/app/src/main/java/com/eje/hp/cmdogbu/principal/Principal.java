package com.eje.hp.cmdogbu.principal;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eje.hp.cmdogbu.Options;
import com.eje.hp.cmdogbu.Util;
import com.eje.hp.cmdogbu.VolleySingleton;
import com.eje.hp.cmdogbu.clases.PanelPublicidad;
import com.eje.hp.cmdogbu.clases.Usuario;
import com.eje.hp.cmdogbu.clases.Usuarios;

public class Principal extends AppCompatActivity implements View.OnClickListener {

    ProgressDialog progreso;
    private SharedPreferences prefs;

    EditText TXT_USR,TXT_PASS;
    public CheckBox rbRecordar;
    Button btnIngresar;
    boolean band=false;

    private Typeface ligth;
    public static final int MY_DEFAULT_TIMEOUT = 150000;
    SharedPreferences.Editor editor;


    static final Integer PHONESTATS = 0x1;
    private final String TAG=Principal.class.getSimpleName();
    String imei;
    Usuarios Usu = new Usuarios();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.eje.hp.cmdogbu.R.layout.activity_principal);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        prefs = getSharedPreferences("Preferences",Context.MODE_PRIVATE);
        IniciarComponentes();
        consultarPermiso(Manifest.permission.READ_PHONE_STATE, PHONESTATS);
        setCredentialsIfExist();
        btnIngresar.setOnClickListener(this);
    }

    private void IniciarComponentes(){
        String fuente = "fuentes/ligth.otf";
        this.ligth = Typeface.createFromAsset(getAssets(),fuente);
        TXT_USR = (EditText)findViewById(com.eje.hp.cmdogbu.R.id.Codigo);
        TXT_USR.setTypeface(ligth);
        TXT_PASS = (EditText)findViewById(com.eje.hp.cmdogbu.R.id.Password);
        TXT_PASS.setTypeface(ligth);
        rbRecordar = (CheckBox) findViewById(com.eje.hp.cmdogbu.R.id.rbRe);
        rbRecordar.setTypeface(ligth);
        btnIngresar = (Button)findViewById(com.eje.hp.cmdogbu.R.id.btnIngresar);
        btnIngresar.setTypeface(ligth);
        editor = prefs.edit();
    }

    private void setCredentialsIfExist(){
        String Usuario = Util.getUsuario(prefs);
        String Password = Util.getPassword(prefs);
        if(!TextUtils.isEmpty(Usuario) && !TextUtils.isEmpty(Password)){
            TXT_PASS.setText(Password);
            TXT_USR.setText(Usuario);
        }
    }

    private void saveOnPreferences(String Usuario,String Password,String ApellidoPa,String ApellidoMa,String Nombre,String Sexo,int fkEscuela,String Dni,int fkSitAcademica,int fkEstPermanencia,String Observacion,Intent intent){
        if(rbRecordar.isChecked())
        {
            editor.putString("Usuario",Usuario);
            editor.putString("Apellidos",ApellidoPa+" "+ApellidoMa);
            editor.putString("Nombres",Nombre);
            editor.putString("fkEscuela", String.valueOf(fkEscuela));
            editor.putString("Dni",Dni);
            editor.putString("fkSitAcademica", String.valueOf(fkSitAcademica));
            editor.putString("fkEstPermanencia", String.valueOf(fkEstPermanencia));
            editor.putString("Observacion",Observacion);
            editor.putString("Password",Password);
            editor.putBoolean("Check",true);
            editor.apply();
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }else{
            editor.putString("Usuario",Usuario);
            editor.putString("Apellidos",ApellidoPa+" "+ApellidoMa);
            editor.putString("Nombres",Nombre);
            editor.putString("fkEscuela", String.valueOf(fkEscuela));
            editor.putString("Dni",Dni);
            editor.putString("fkSitAcademica", String.valueOf(fkSitAcademica));
            editor.putString("fkEstPermanencia", String.valueOf(fkEstPermanencia));
            editor.putString("Observacion",Observacion);
            editor.putString("Password",Password);
            editor.putBoolean("Check",false);
            editor.apply();
        }
    }

    @Override
    public void onClick(View v) {
        if(TXT_USR.getText().toString().equalsIgnoreCase("") && !TXT_PASS.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(Principal.this, "Codigo universitario invalido",Toast.LENGTH_LONG).show();
        }else if(!TXT_USR.getText().toString().equalsIgnoreCase("") && TXT_PASS.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(Principal.this, "Contraseña invalida",Toast.LENGTH_LONG).show();
        }else if(TXT_USR.getText().toString().equalsIgnoreCase("") && TXT_PASS.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(Principal.this, "Datos invalidos",Toast.LENGTH_LONG).show();
        }else if(!TXT_USR.getText().toString().equalsIgnoreCase("") && !TXT_PASS.getText().toString().equalsIgnoreCase("")) {
            try {
                CargarUsuarios();
            } catch (Exception e) {
                progreso.dismiss();
                e.printStackTrace();
            }
        }
    }

    private void CargarUsuarios() throws Exception {
        JsonObjectRequest jsonObjectRequestP;
        progreso = new ProgressDialog(this);
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"consultarUsuario.php?alu_Codigo="+TXT_USR.getText().toString();
        jsonObjectRequestP= new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("alumno");
                try {
                    JSONObject jsonObject;
                    jsonObject = json.getJSONObject(0);
                    Usu.setCodigo(jsonObject.optString("alu_Codigo"));
                    Usu.setApePaterno(jsonObject.optString("alu_ApePaterno"));
                    Usu.setApeMaterno(jsonObject.optString("alu_ApeMaterno"));
                    Usu.setNombre(jsonObject.optString("alu_Nombre"));
                    Usu.setFkEscuela(jsonObject.optInt("alu_fkEscuela"));
                    Usu.setDni(jsonObject.optString("alu_Dni"));
                    Usu.setFkSitAcademica(jsonObject.optInt("alu_fkSitAcademica"));
                    Usu.setFkEstPermanencia(jsonObject.optInt("alu_fkEstPermanencia"));
                    Usu.setObservacion(jsonObject.optString("alu_Observacion"));
                    if(Usu.getCodigo().equalsIgnoreCase(TXT_USR.getText().toString()) && Usu.getDni().equalsIgnoreCase(TXT_PASS.getText().toString())  && (Usu.getFkEstPermanencia()==4)){
                        band=true;
                        editor.putString("Usuario",Usu.getCodigo());
                        editor.apply();
                        try{
                            progreso.dismiss();
                            VerificarImei();
                        }catch (Exception e){
                            progreso.dismiss();
                            e.printStackTrace();
                        }
                    }
                    if(!band){
                        Toast.makeText(Principal.this, "No se encontró al usuario",Toast.LENGTH_LONG).show();
                        progreso.dismiss();
                    }
                } catch (JSONException e) {
                    Toast.makeText(Principal.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                    progreso.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(Principal.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestP.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstanciaVolley(Principal.this).addToRequestQueue(jsonObjectRequestP);
    }

    private void VerificarImei() throws Exception {
        JsonObjectRequest jsonObjectRequestP;
        progreso = new ProgressDialog(this);
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"verificarImei.php?ctr_Codigo="+Util.getUsuario(prefs)+"&ctr_Imei="+Util.getImei(prefs);
        jsonObjectRequestP= new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("control_2019");
                Usuario mu = new Usuario();
                JSONObject jsonObject=null;
                try {
                    jsonObject = json.getJSONObject(0);
                    mu.setCodigo(jsonObject.optString("alui_codigo"));
                    mu.setImei(jsonObject.optString("alui_imei"));
                    mu.setId(jsonObject.optInt("alui_bloqueo"));
                    if (mu.getId() == 1) {
                        if (mu.getImei().equalsIgnoreCase(Util.getImei(prefs)) || mu.getImei().equalsIgnoreCase("0")) {
                            if (mu.getCodigo().equalsIgnoreCase(Util.getUsuario(prefs)) || mu.getCodigo().equalsIgnoreCase("0")) {
                                progreso.dismiss();
                                Intent intent = new Intent(Principal.this, Options.class);
                                saveOnPreferences(Usu.getCodigo(), Usu.getContrasena(), Usu.getApePaterno(), Usu.getApeMaterno(), Usu.getNombre(), Usu.getSexo(), Usu.getFkEscuela(), Usu.getDni(), Usu.getFkSitAcademica(), Usu.getFkEstPermanencia(), Usu.getObservacion(), intent);
                                startActivity(intent);
                            } else {
                                progreso.dismiss();
                                Toast.makeText(Principal.this, "No puede entrar otro usuario desde su celular", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            progreso.dismiss();
                            Toast.makeText(Principal.this, "Usted posee otro celular registrado a su nombre", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        progreso.dismiss();
                        Toast.makeText(Principal.this, "Usuario bloqueado para sacar ticket por aplicacion"+mu.getId(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Toast.makeText(Principal.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                    progreso.dismiss();
                } catch (Exception e) {
                    Toast.makeText(Principal.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                    progreso.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(Principal.this, "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestP.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleySingleton.getInstanciaVolley(Principal.this).addToRequestQueue(jsonObjectRequestP);
    }

    // Con este método mostramos en un Toast con un mensaje que el usuario ha concedido los permisos a la aplicación
    private void consultarPermiso(String permission, Integer requestCode) {
        if (ActivityCompat.checkSelfPermission(Principal.this, permission) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(Principal.this, permission)) {

                ActivityCompat.requestPermissions(Principal.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(Principal.this, new String[]{permission}, requestCode);
            }
        } else {
            imei = obtenerIMEI();
            editor.putString("Imei",imei);
            editor.apply();
        }
    }

    // Con este método consultamos al usuario si nos puede dar acceso a leer los datos internos del móvil
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case 1: {
                // Validamos si el usuario acepta el permiso para que la aplicación acceda a los datos internos del equipo, si no denegamos el acceso
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    imei = obtenerIMEI();
                    editor.putString("Imei",imei);
                    editor.apply();
                    Toast.makeText(this,"Acceso permitido",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Principal.this, "Negaste el permiso de la aplicación, ir Acerca del Programa para mas información", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @SuppressLint("MissingPermission")
    private String obtenerIMEI() {
        final TelephonyManager telephonyManager= (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Hacemos la validación de métodos, ya que el método getDeviceId() ya no se admite para android Oreo en adelante, debemos usar el método getImei()
            return telephonyManager.getImei();
        }
        else {
            return telephonyManager.getDeviceId();
        }

    }

}
