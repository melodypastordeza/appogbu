package com.eje.hp.cmdogbu.reservar_ticket;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.eje.hp.cmdogbu.Options;
import com.eje.hp.cmdogbu.Util;
import com.eje.hp.cmdogbu.clases.PanelPublicidad;
import com.eje.hp.cmdogbu.clases.Tickets;
import com.eje.hp.cmdogbu.mostrar_ticket.FragmentMostrarT;


public class FragmentReservarT extends Fragment implements FragmentMostrarT.OnFragmentInteractionListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    String[] turnos1 = new String[5];
    String[] turnos2 = new String[5];
    TextView texto1,texto2;
    ListView lista1,lista2;
    ProgressDialog progreso;
    private int numero;

    public static final int MY_DEFAULT_TIMEOUT = 150000;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentReservarT() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentReservarT.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentReservarT newInstance(String param1, String param2) {
        FragmentReservarT fragment = new FragmentReservarT();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(com.eje.hp.cmdogbu.R.layout.fragment_fragment_reservar_t, container, false);
        InicializarComponentes(rootView);
        try {
            SubirTickets();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootView;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void InicializarComponentes(View rootView){
        lista1=(ListView)rootView.findViewById(com.eje.hp.cmdogbu.R.id.lista1);
        lista2=(ListView)rootView.findViewById(com.eje.hp.cmdogbu.R.id.lista2);
        texto1=(TextView)rootView.findViewById(com.eje.hp.cmdogbu.R.id.textView01);
        texto2=(TextView)rootView.findViewById(com.eje.hp.cmdogbu.R.id.textView02);
        texto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lista1.setVisibility(View.VISIBLE);
            }
        });
        texto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lista2.setVisibility(View.VISIBLE);
            }
        });
    }

    private void GuardarUsuario(int nivel,int turno) throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestGU = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequestGU;
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"guardarUsuario.php?ctr_fkAlumno="+Util.getUsuario(prefs)+"&ctr_fkComedor="+8+"&ctr_fkServicio="+12+"&ctr_Nivel="+nivel+"&ctr_Turno="+turno+"&ctr_fkUsuRegistro="+100+"&ctr_Observacion="+Util.getImei(prefs);
        add=add.replace(" ","%20");
        jsonObjectRequestGU = new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Crea el nuevo fragmento y la transacción.
                progreso.dismiss();
                Options.rsp = true;
                Toast.makeText(getContext(), "Ticket Reservado", Toast.LENGTH_SHORT).show();
                FragmentMostrarT fragment = new FragmentMostrarT();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(com.eje.hp.cmdogbu.R.id.fragmentReservarTicket, fragment, "FragmentMostrarT");
                ft.commit();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestGU.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestGU.add(jsonObjectRequestGU);
    }

    private void SubirTickets() throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestST = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequestST;
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"subirTicket.php";
        jsonObjectRequestST = new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Tickets[] ti = new Tickets[10];
                JSONArray json = response.optJSONArray("ticket");
                try {
                    for (int i = 0; i < json.length(); i++) {
                        JSONObject jsonObject;
                        jsonObject = json.getJSONObject(i);
                        ti[i] = new Tickets();
                        ti[i].setTi_id(jsonObject.optInt("ti_id"));
                        ti[i].setTi_nivel(jsonObject.optInt("ti_nivel"));
                        ti[i].setTi_turno(jsonObject.optInt("ti_turno"));
                        ti[i].setTi_cantidad(jsonObject.optInt("ti_cantidad"));
                    }
                    progreso.dismiss();
                    ItemsLista(ti);
                } catch (JSONException e) {
                    progreso.dismiss();
                    Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestST.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestST.add(jsonObjectRequestST);
    }

    public void ItemsLista(Tickets tickets[]){
        int suma1=0,suma2=0;

        for (int i=0;i<tickets.length;i++){
            if(tickets[i].getTi_nivel()==1) {
                turnos1[i] = "Turno : " + (i + 1) + "\n" + "Cantidad : " + tickets[i].getTi_cantidad();
                if(tickets[i].getTi_cantidad()>0){
                    suma1=suma1+tickets[i].getTi_cantidad();
                }
            }else{
                turnos2[i-5]="Turno : "+(i-4)+"\n"+"Cantidad : "+tickets[i].getTi_cantidad();
                if(tickets[i].getTi_cantidad()>0){
                    suma2=suma2+tickets[i].getTi_cantidad();
                }
            }
        }

        texto1.setText("Nivel : 1"+"\n"+"Raciones : "+suma1);
        texto2.setText("Nivel : 2"+"\n"+"Raciones : "+suma2);


        for(int i=0;i<5;i++) {
            if(tickets[i].getTi_cantidad() <= 0) {
                turnos1[i] = "";
            }
        }

        for(int i=0;i<5;i++) {
            if (tickets[i + 5].getTi_cantidad() <= 0){
                turnos2[i] = "";
            }
        }

        ArrayAdapter adapter1 = new ArrayAdapter(getActivity(), com.eje.hp.cmdogbu.R.layout.list_item1,turnos1);
        ArrayAdapter adapter2 = new ArrayAdapter(getActivity(), com.eje.hp.cmdogbu.R.layout.list_item1,turnos2);

        lista1.setAdapter(adapter1);
        lista2.setAdapter(adapter2);

        lista1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position){
                    case 0:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=1;
                        break;
                    case 1:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=2;
                        break;
                    case 2:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=3;
                        break;
                    case 3:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=4;
                        break;
                    case 4:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=5;
                        break;
                }
            }});
        lista2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position){
                    case 0:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=6;
                        break;
                    case 1:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=7;
                        break;
                    case 2:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=8;
                        break;
                    case 3:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=9;
                        break;
                    case 4:
                        try {
                            AumentoDeCantidad();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        numero=10;
                        break;
                }
            }});
    }

    private void ConsultarTicket(int i) throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestC = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequestC;
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"consultarTicket.php?ti_id="+i;
        add=add.replace(" ","%20");
        jsonObjectRequestC = new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("ticket");
                try {
                    JSONObject jsonObject;
                    jsonObject = json.getJSONObject(0);
                    int id = jsonObject.optInt("ti_id");
                    int cantidad = jsonObject.optInt("ti_cantidad");
                    int turno = jsonObject.optInt("ti_turno");
                    int nivel = jsonObject.optInt("ti_nivel");
                    int activodia=jsonObject.optInt("activodia");
                    if(cantidad>0&&activodia==1) {
                        progreso.dismiss();
                        GuardarUsuario(nivel,turno);
                    }else{
                        progreso.dismiss();
                        Toast.makeText(getContext(), "No se a podido reservar ticket", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    progreso.dismiss();
                    Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    progreso.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestC.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestC.add(jsonObjectRequestC);
    }

    private void AumentoDeCantidad() throws Exception {
        SharedPreferences prefs = getActivity().getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        RequestQueue requestAC = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequestAC;
        progreso = new ProgressDialog(getContext());
        progreso.setMessage("Consultando...");
        progreso.show();
        PanelPublicidad s = new PanelPublicidad();

        String mov=s.Despublicidad(Util.getSexo(prefs),Util.getGenero(prefs));
        String add = mov+"AumentoCantidad.php";
        add=add.replace(" ","%20");
        jsonObjectRequestAC= new JsonObjectRequest(Request.Method.GET, add, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("app_id");
                try {
                    JSONObject jsonObject;
                    jsonObject = json.getJSONObject(0);
                    int app_cantidadtotal = jsonObject.optInt("app_cantidadtotal");
                    int app_cantidadconsumida =jsonObject.optInt("app_cantidadconsumida");
                    if(app_cantidadconsumida<app_cantidadtotal) {
                        progreso.dismiss();
                        ConsultarTicket(numero);
                    }else{
                        progreso.dismiss();
                        Toast.makeText(getContext(), "No se a podido reservar ticket", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    progreso.dismiss();
                    Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    progreso.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progreso.dismiss();
                Toast.makeText(getContext(), "Eeeh...\nNo estás conectado a internet", Toast.LENGTH_LONG).show();
            }
        });
        jsonObjectRequestAC.setRetryPolicy(new DefaultRetryPolicy(MY_DEFAULT_TIMEOUT,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestAC.add(jsonObjectRequestAC);
    }
}
