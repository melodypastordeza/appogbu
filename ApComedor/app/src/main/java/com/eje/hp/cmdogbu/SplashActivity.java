package com.eje.hp.cmdogbu;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.eje.hp.cmdogbu.principal.Main;

public class SplashActivity extends AppCompatActivity {

    private SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        prefs = getSharedPreferences("Preferences", Context.MODE_PRIVATE);
        Intent intentPrincipal = new Intent(this,Main.class);
        Intent intentOptions = new Intent(this,Options.class);

        if(Util.getCheck(prefs)){
            startActivity(intentOptions);
        }else {
            startActivity(intentPrincipal);
        }
        finish();
    }

}

