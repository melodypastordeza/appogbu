package com.eje.hp.cmdogbu.principal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.eje.hp.cmdogbu.clases.PanelPublicidad;
import com.eje.hp.cmdogbu.publicidad.AcercaPrograma;

public class Main extends AppCompatActivity implements View.OnClickListener{

    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Button btnIN,btnAP;
    private Typeface ligth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.eje.hp.cmdogbu.R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        prefs = getSharedPreferences("Preferences",Context.MODE_PRIVATE);
        IniciarComponentes();
        PanelPublicidad s = new PanelPublicidad();
        try {
            String NumDni = "123456789";
            String p = s.Publicidad(getString(com.eje.hp.cmdogbu.R.string.ip),NumDni);
            editor.putString("Sexo",p);
            editor.putString("Genero",NumDni);
            editor.apply();

        } catch (Exception e) {
            e.printStackTrace();
        }
        btnIN.setOnClickListener(this);
        btnAP.setOnClickListener(this);
    }

    private void IniciarComponentes(){
        String fuente = "fuentes/ligth.otf";
        this.ligth = Typeface.createFromAsset(getAssets(),fuente);
        btnIN = (Button)findViewById(com.eje.hp.cmdogbu.R.id.btnIn);
        btnIN.setTypeface(ligth);
        btnAP = (Button)findViewById(com.eje.hp.cmdogbu.R.id.btnAp);
        btnAP.setTypeface(ligth);
        editor = prefs.edit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case com.eje.hp.cmdogbu.R.id.btnIn:{
                Intent intent = new Intent(Main.this,Principal.class);
                startActivity(intent);
                break;
            }

            case com.eje.hp.cmdogbu.R.id.btnAp:{
                Intent intent = new Intent(Main.this,AcercaPrograma.class);
                startActivity(intent);
                break;
            }

        }
    }
}
